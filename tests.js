console.log('All tests passed if this program ends with == OK ==');

var jsonDB = require('./lib/jsonDB').jsonDB;
var db = new jsonDB({logging:false});
var dbName = 'testDB/testdb.json';

db.connect(dbName, function(err) {
    if (err) throw 'Connect failed:' + err;

    runInsertTest();
});



var runInsertTest = function() {
    var timeStamp = new Date().toString();
    
    var random = Math.floor(Math.random() * 1000);
    
    var title = timeStamp + '[' + random + ']'
    
    var newDoc = {title: title, field2: 'two', field3: 'three'};
    
    db.insert(newDoc, function(err, insertedData) {
        if(err) throw 'Insert failed: ' + err;
        if(insertedData.title != title) throw 'Unexpected title found';
        runFind();
    });
    
    console.log(db);
}



var runFind = function() {
    db.find(function(docs) {
        console.log(JSON.stringify(docs, null, 4));
        runAllTestPassed();
    });
}

var runAllTestPassed = function() {
    console.log('== OK ==');
}
