/*--------------------------------------------------------------
*  Copyright (c) TheYkk(Yusuf Kaan Karakaya <yusufkaan142@gmail.com>). All rights reserved.
*  Licensed under the MIT License.
*-------------------------------------------------------------*/
var path = require('path');
var fs = require('fs');
var util = require('./dbUtil');

var jsonDB = function(options) {
    this.dbFile = null;
    
    this.Data = {};
    this.log = function(message) {};
    
    if(options != null && options.logging === true) {
        this.log = function(message) {
            console.log('jsonDB: %s', message);
        }
    }
}


// ----------------------------------
// Connect to a database
// ----------------------------------
jsonDB.prototype.connect = function(dbFile, cb) {
    this.log('Connecting to: ' + dbFile);
    this.dbFile = dbFile;
    
    var _this = this;
    
    // Check if the path exists and it's indeed a directory.
    fs.exists(_this.dbFile, function(exists) {
        if (exists) {
            
            fs.readFile(_this.dbFile, function(err, data) {
                if(err) {
                    _this.log(''+_this.dbFile+' file already exists, but could not be read.');
                    cb(err);
                }
                else {
                    _this.log(''+_this.dbFile+'  file read');
                    
                    try {
                        _this.Data = JSON.parse(data);
                    } catch (error) {
                        
                    }
                    
                    cb(null);
                }
            });
            
        }
        else {
            
            // create index file
            _this.log('Creating file: ' + _this.dbFile);
            fs.writeFile(_this.dbFile, "", function(err) {
                if (err) {
                    _this.log('ERROR', 'Could not create   file. Error: ' + err);
                    cb(err);
                }
                else {
                    _this.log(''+_this.dbFile+' file created');
                    cb(null);
                }
            });
            
        }
    });
}




// ----------------------------------------------------
// Inserts a new document in the database
// data is an object with the values to save.
// ----------------------------------------------------
jsonDB.prototype.insert = function(data, cb) {
    this.log('About to insert');
    
    this.Data = util.merge( this.Data , data );
    try {
        var documentFile = this.dbFile;
        fs.writeFileSync(documentFile, JSON.stringify( this.Data ));
        this.log('Document inserted: ' + documentFile);
        
        cb(null, this.Data);
    }
    catch(err) {
        this.log('Error inserting:' + err);
        cb(err);
    }
    
}

// ----------------------------------
// Find documents in the database
// ----------------------------------
jsonDB.prototype.find = function(cb) {
    
    cb(this.Data);
    this.log(JSON.stringify(this.Data, null, 4));
}

    exports.jsonDB = jsonDB;